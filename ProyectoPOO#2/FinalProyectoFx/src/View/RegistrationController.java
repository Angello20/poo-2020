package View;

import java.io.File;
import java.io.IOException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import Idea.JSONParser;
import Idea.Alerta;
import Idea.Ubicacion;
import Idea.Usuario;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;


public class RegistrationController {


	@FXML
	private TextField nombre;
	
	@FXML
	private TextField provincia;
	
	@FXML
	private TextField canton;
	
	@FXML
	private TextField distrito;
	
	@FXML
	private TextField placa;
	
	@FXML
	private Label Error;
	
	@FXML
	private void initialize() 
	{
		
	}
	
	
	public void cambiarInicio(ActionEvent event ) throws IOException
	{
		if(registroUsuario()==true) {
		Parent InicioD = FXMLLoader.load(getClass().getResource("Inicio.fxml"));
		Scene Inicio = new Scene(InicioD);
		
		Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
		window.setScene(Inicio);
		window.show();
		}
		else {
		}
	}
	
	public boolean registroUsuario() throws JsonProcessingException, IOException
	{
		if(verificarCampos()==true) {
			String nom = nombre.getText();
			String prov = provincia.getText();
			String cant = canton.getText();
			String dist = distrito.getText();
			String pla = placa.getText();
			int plac;
			try {
				
					plac = Integer.parseInt(placa);
					Alerta alerta;
					JSONParser registroParser = new JSONParser();
					if (registroParser.buscar(prov, cant, dist) == true) {
						alerta = registroParser.obtenerAlerta(0, plac);
						
					}
					else {
						alerta = registroParser.obtenerAlerta(1, plac);
						
					}
					Ubicacion lugar = new Ubicacion(prov,cant,dist);
					
					
					
					Usuario user = new Usuario(nom,lugar,plac,alerta);
					ObjectMapper mapper = new ObjectMapper();
		            File file = new File("data/users.json");
		            mapper.configure(SerializationFeature.INDENT_OUTPUT, true);
		            JsonNode nodo = mapper.readTree(file);
		            ArrayNode usuarios = (ArrayNode) nodo.get("usuarios");
		            JsonNode newNode = mapper.valueToTree(user);
		            usuarios.add(newNode);
		            ((ObjectNode) nodo).set("usuarios", usuarios);
		            mapper.writeValue(file, nodo);
		            return true;
				}
				catch (NumberFormatException e)
				{
					Error.setText("Llene los espacios");
					return false;
				}
		}
		else {
			Error.setText("Llene los espacios");
			return false;
		}
		
	}

	public boolean verificarCampos() {
		if(nombre.getText().trim().isEmpty() || provincia.getText().trim().isEmpty()  || canton.getText().trim().isEmpty()  || distrito.getText().trim().isEmpty() || placa.getText().trim().isEmpty()) {
			return false;
		}
		else {
			return true;
		}
	}
}