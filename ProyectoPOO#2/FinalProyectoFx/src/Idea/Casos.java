package Idea;

public class Casos {
	private String Pais;
	private String Fecha;
	private int Acumulados;
	private int Activos;
	private int Muertes;
	private int Recuperados;
	private float TasaRecuperacion;
	private float TasaMortalidad;
	
	public Casos(String pais, String fecha, int acumulados, int muertes, int recuperados) {
		Pais = pais;
		Fecha = fecha;
		Acumulados = acumulados;
		this.Activos = (acumulados-(muertes+recuperados));
		this.TasaRecuperacion = (recuperados%acumulados)/100 ;
		this.TasaMortalidad = (muertes%acumulados) /100;
		Muertes = muertes;
		Recuperados = recuperados;

	}
	
	public String getPais() {
		return Pais;
	}
	
	public void setPais(String pais) {
		Pais = pais;
	}
	
	public String getFecha() {
		return Fecha;
	}
	
	public void setFecha(String fecha) {
		Fecha = fecha;
	}
	
	public int getAcumulados() {
		return Acumulados;
	}
	
	public void setAcumulados(int acumulados) {
		Acumulados = acumulados;
	}
	
	public int getActivos() {
		return Activos;
	}
	
	public void setActivos(int activos) {
		Activos = activos;
	}
	
	public int getMuertes() {
		return Muertes;
	}
	
	public void setMuertes(int muertes) {
		Muertes = muertes;
	}
	
	public int getRecuperados() {
		return Recuperados;
	}
	
	public void setRecuperados(int recuperados) {
		Recuperados = recuperados;
	}
	
	public float getTasaRecuperacion() {
		return TasaRecuperacion;
	}
	
	public void setTasaRecuperacion(float tasaRecuperacion) {
		TasaRecuperacion = tasaRecuperacion;
	}
	
	public float getTasaMortalidad() {
		return TasaMortalidad;
	}
	
	public void setTasaMortalidad(float tasaMortalidad) {
		TasaMortalidad = tasaMortalidad;
	}
	
	


}
