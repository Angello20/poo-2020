package application;

import java.net.URL;
import java.util.ResourceBundle;

import javax.swing.JOptionPane;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class Controlador {
	
	 @FXML
	 private Label label;
	 
	 @FXML
	 private TextField inputTexto;
	 
	 public void mostrarTexto(ActionEvent event) {
		 String texto = inputTexto.getText();
		 label.setText(texto);
	 }
}
