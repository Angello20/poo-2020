package ch.makery.address.view;

import idea.JSONParser;
import idea.ListaDeCasos;
import idea.ListaUsuarios;
import javafx.fxml.FXML;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;



public class DisplayController {
	//Sesion actual
	private int Sesion;
	
	//Info de Usuario
	@FXML
    private Label Nombre;
	
	@FXML
    private Label Lugar;
    
    @FXML
    private Label Placa;
    
    @FXML
    private Label Alerta;
 
    //Info del chart
    
    @FXML
    private LineChart<?,?> chart;
    
    @FXML
    private CategoryAxis x;
    
    @FXML
    private NumberAxis y;
    
    //Info Tasas
    
    @FXML
    private Label Mortalidad;
    @FXML
    private Label Recuperacion;
    
    
    
    
    

    @FXML
    private void initialize() {}
    
    
    private void establecerInfo() {
    	//parser para cargar
    	JSONParser parser = new JSONParser();
    	
    	ListaDeCasos casos = parser.cargarCasos();
    	
    	//destinado al chart
    	XYChart.Series Acumulados = new XYChart.Series<>();
    	XYChart.Series Activos = new XYChart.Series<>();
    	XYChart.Series Recuperados = new XYChart.Series<>();
    	int contador = 49;
    	while(contador > -1) {
    		
    		
    		Acumulados.setName("Acumulados");
    		Activos.setName("Activos");
    		Recuperados.setName("Recuperados");
    		
    		
    		Acumulados.getData().add(new XYChart.Data<>(casos.get(contador).getFecha(),casos.get(contador).getAcumulados()));
    		Activos.getData().add(new XYChart.Data<>(casos.get(contador).getFecha(),casos.get(contador).getActivos()));
    		Recuperados.getData().add(new XYChart.Data<>(casos.get(contador).getFecha(),casos.get(contador).getRecuperados()));
    		
    		contador = contador-1;
    		
    	}
    	chart.getData().addAll(Acumulados,Activos,Recuperados);
    	
    	//label de porcentajes
    	
    	Mortalidad.setText("La tasa de mortalidad es: "+casos.get(0).getTasaMortalidad()+"%");
    	Recuperacion.setText("La tasa de recuperacion es: "+casos.get(0).getTasaRecuperacion()+"%");
		
  
    	//establece info segun la sesion.
    	ListaUsuarios usuarios = parser.cargarUsuarios();
    	
    	Nombre.setText(usuarios.get(Sesion).getNombre());
    	
    	
        Lugar.setText(usuarios.get(Sesion).getLugar().toString() + " se encuentra en " + usuarios.get(Sesion).getAlerta().getTipoAlerta()+".");
        
        int recorreEstablecimientos = 0;
        String alertatemp = "";
        while(recorreEstablecimientos < usuarios.get(Sesion).getAlerta().getEstablecimientos().size()) { 	
        	alertatemp += ("•" + usuarios.get(Sesion).getAlerta().getEstablecimientos().get(recorreEstablecimientos) +".\n");
        	recorreEstablecimientos += 1;
        }
        Alerta.setText(alertatemp);
        //pasar a string un int xd
        int placatemp = usuarios.get(Sesion).getPlaca();
        String placa = String.valueOf(placatemp);
        
        int recorreRestricciones = 0;
        String restriccion = "";
        while(recorreRestricciones < usuarios.get(Sesion).getAlerta().getRestriccionVehicular().size()) {
        	restriccion += ("•" + usuarios.get(Sesion).getAlerta().getRestriccionVehicular().get(recorreRestricciones) +".\n");
        	recorreRestricciones += 1;
        }
        Placa.setText("El vehiculo con la placa " + placa + " puede circular: \n" + restriccion);
    }

    
    //se accede a este metodo desde el controlador inicio, para definir una sesion ademas de establecer la info pertinente a los labels.
    public void determinarSesion(int Sesion) {
    	this.Sesion = Sesion;
    	establecerInfo();
    }

}



