import java.time.LocalDate;


public class Cuenta {
	
	//atributos de la clase cuenta
	private int id;
	private double balance;
	private double tasaDeInteresAnual;
	private LocalDate fechaDeCreacion;
	private int dineroRetirado;
	private int dineroDepositado;
	
	//constructor vacío de la cuenta
	public Cuenta() {
		
	}
	
	//constructor de la cuenta
	public Cuenta(int id, double balance, double tasaDeInteresAnual, LocalDate fechaDeCreacion) {
		this.id = id;
		this.balance = balance;
		this.tasaDeInteresAnual = tasaDeInteresAnual;
		this.fechaDeCreacion = fechaDeCreacion;
	}
	
	//metodo setter de la id
	public void setid(int id) {
		this.id = 0;
	}
	
	//metodo getter de la id
	public int getId() {
		return id;
	}
	
	//metodo setter del balance
	public void setBalance(double balance) {
		this.balance = balance;
	}
	
	//metodo getter del balance
	public double getBalance() {
		return balance;
	}
	
	//metodo setter de la Tasa de interes anual
	public void setTasaDeInteresAnual(double tasaDeInteresAnual) {
		this.tasaDeInteresAnual = tasaDeInteresAnual;
	}
	
	//metodo getter de la Tasa de interes anual
	public double getTasaDeInteresAnual() {
		return tasaDeInteresAnual;
	}
	
	//metodo que crea la fecha de creacion de la cuenta
	public LocalDate getfechaDeCreacion() {
		return fechaDeCreacion;
	}
	
	//metodo para retirar dinero de la cuenta
	public double retirarDinero(int dineroRetirado) {
		return balance = balance - dineroRetirado;
	}
	
	//metodo para depositar a la cuenta 
	public double depositarDinero(int dineroDepositado) {
		return balance = balance + dineroDepositado;
	}
}

