package Idea;
public class Usuario {
	private String Nombre;
	private Ubicacion Lugar;
	private int Placa;
	private Alerta Alerta;
	
	public Usuario(String Nombre,Ubicacion Lugar,int Placa,Alerta Alerta) {
		this.Nombre= Nombre;
		this.Lugar = Lugar;
		this.Placa = Placa;
		this.Alerta = Alerta;
	}

	public String getNombre() {
		return Nombre;
	}

	public void setNombre(String nombre) {
		Nombre = nombre;
	}

	public Ubicacion getLugar() {
		return Lugar;
	}

	public void setLugar(Ubicacion lugar) {
		Lugar = lugar;
	}

	public int getPlaca() {
		return Placa;
	}

	public void setPlaca(int placa) {
		Placa = placa;
	}

	public Alerta getAlerta() {
		return Alerta;
	}

	public void setAlerta(Alerta alerta) {
		Alerta = alerta;
	}
	
	

}
