package Idea;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import Idea.Alerta;
import Idea.Casos;
import Idea.Ubicacion;
import Idea.Usuario;


public class JSONParser {
	private File listaUsuarios;
	private File listaAlertas;
	private ObjectMapper mapperUsuarios;
	private ObjectMapper mapperAlertas;
	private ObjectMapper mapperCasos;
	private JsonNode nodoUsuarios;
	private JsonNode nodoAlertas;
	private JsonNode nodoCasos;
	
	private void inicioUsuarios() throws JsonProcessingException, IOException {
		File tempUsuarios = new File("data/users.json");
		this.listaUsuarios = tempUsuarios;
		this.mapperUsuarios = new ObjectMapper();
		this.nodoUsuarios = mapperUsuarios.readTree(listaUsuarios);
	}
	
	private void inicioAlertas() throws JsonProcessingException, IOException {
		File tempAlertas = new File("data/alertas.json");
		this.listaAlertas = tempAlertas;
		this.mapperAlertas = new ObjectMapper();
		this.nodoAlertas = mapperAlertas.readTree(listaAlertas);

	}

	private void inicioCasos() throws JsonProcessingException, IOException {
		URL url = new URL("https://covid19-api.org/api/timeline/CR");
		this.mapperCasos = new ObjectMapper();
		this.nodoCasos = mapperCasos.readTree(url);
		
	}
	
	public JSONParser() {
		try {
			inicioUsuarios();
			inicioAlertas();
			inicioCasos();
		} 
		
		catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}

		public ListaDeCasos cargarCasos() {
			ListaDeCasos Cases = new ListaDeCasos();

			ArrayNode arregloCasos = (ArrayNode) nodoCasos;
			
			if (arregloCasos != null) {
	              for(int i=0; i<50;i++) {
	                  JsonNode caso = arregloCasos.get(i);
	                  int acumulados = caso.get("cases").asInt();
	                  String fecha =caso.get("last_update").asText();
	                  int muertes = caso.get("deaths").asInt();
	                  int recuperados = caso.get("recovered").asInt();
	                  String pais = caso.get("country").asText();
	                  Casos cso = new Casos(pais,fecha,acumulados,muertes,recuperados);
	                  Cases.add(cso);
	              }
	          }
	          return Cases;		
		}

		
		public ListaUsuarios cargarUsuarios() {
			ListaUsuarios usuarios = new ListaUsuarios();
			ArrayNode arregloUsuarios = (ArrayNode) nodoUsuarios.get("usuarios");
			if(arregloUsuarios != null) {
				for(int i=0; i<arregloUsuarios.size();i++) {
					JsonNode usuario = arregloUsuarios.get(i);
					String nombre = usuario.get("nombre").asText();
					JsonNode nodoLugar = arregloUsuarios.get(i).get("lugar");
					String provincia = nodoLugar.get("provincia").asText();
					String canton = nodoLugar.get("canton").asText();
					String distrito = nodoLugar.get("distrito").asText();
					
					Ubicacion lugar = new Ubicacion(provincia,canton,distrito);
					
					int placa = usuario.get("placa").asInt();
					JsonNode nodoAlerta = arregloUsuarios.get(i).get("alerta");
					String tipoAlerta = nodoAlerta.get("tipoAlerta").asText();
					ArrayList<String> establecimientos = new ArrayList<String>();					
					int recorrerEstablecimientos = 0;
					while(recorrerEstablecimientos < nodoAlerta.get("establecimientos").size()) {
						establecimientos.add(nodoAlerta.get("establecimientos").get(recorrerEstablecimientos).asText());
						recorrerEstablecimientos += 1;
					}
					
					ArrayList<String> restricciones = new ArrayList<String>();
					int recorrerRestricciones = 0;
					while(recorrerRestricciones < nodoAlerta.get("restriccionVehicular").size()) {
						restricciones.add(nodoAlerta.get("restriccionVehicular").get(recorrerRestricciones).asText());
						recorrerRestricciones += 1;
					}
					
					Alerta alerta = new Alerta(tipoAlerta,establecimientos,restricciones);
					
					Usuario usr = new Usuario(nombre,lugar,placa,alerta);
					usuarios.add(usr);
					}
				}
			return usuarios;
			}
		
		public int buscarUsuarios(String Usuario) {		
			ArrayNode arregloUsuarios = (ArrayNode) nodoUsuarios.get("usuarios");
			if(arregloUsuarios != null) {
				int i = 0;
				while (i < arregloUsuarios.size()) {
					JsonNode usuario = arregloUsuarios.get(i);
					String nombre = usuario.get("nombre").asText();
					if (nombre.equals(Usuario)) {
						return i;
					}
					else {
						i+=1;
					}
				}
			}
			return -1;
		}
		
		
		public Alerta obtenerAlerta(int indiceTipo,int Placa) {
			int valorPlaca = Placa%10;
			switch(valorPlaca) 
				{
				case 1:
				case 2:{
					valorPlaca = 0;
					break;
				}
				case 3:
				case 4:{
					valorPlaca = 1;
					break;
				}
				case 5:
				case 6:{
					valorPlaca = 2;
					break;
				}
				case 7:
				case 8:{
					valorPlaca = 3;
					break;
				}
				case 9:
				case 0:{
					valorPlaca = 4;
					break;
				}
				}
			
			ArrayNode arregloAlertas = (ArrayNode) nodoAlertas.get("alerta");
			JsonNode alerta = arregloAlertas.get(indiceTipo);
			
			String tipo = alerta.get("tipoAlerta").asText();
			
			ArrayList<String> establecimientos = new ArrayList<String>();
			int indice = 0;
			while(indice < alerta.get("establecimientos").size()) {
				String establecimiento = alerta.get("establecimientos").get(indice).asText();
				establecimientos.add(establecimiento);
				indice+=1;
			}
			
			
			ArrayList <String> restricciones = new ArrayList<String>();
			String restriccionSemana = alerta.get("restriccionSemana").get(valorPlaca).asText();
			restricciones.add(restriccionSemana);
			if(Placa%2 ==0) {
				String restriccionFines = alerta.get("restriccionFines").get(1).asText();
				restricciones.add(restriccionFines);
			}
			else {
				String restriccionFines = alerta.get("restriccionFines").get(0).asText();
				restricciones.add(restriccionFines);
			}
			
			Alerta alertaUsuario = new Alerta(tipo,establecimientos, restricciones);
			return alertaUsuario;
		}
		
		public boolean buscar(String provincia, String canton, String distrito) {
			
			ArrayNode arregloProvincias = (ArrayNode) nodoAlertas.get("provincias"); 
			if(arregloProvincias != null) {
				int i = 0;
				while (i < arregloProvincias.get(0).size()) {
					String key = String.valueOf(i+1);
					JsonNode alertaprov = arregloProvincias.get(0);
					String Provincia = alertaprov.get(key).asText();
					if (Provincia.equals(provincia)) {
						i+=1;
						if(buscarCanton(canton, i) == true & buscarDistrito(distrito)==false ) {
							return true;
						}
						else {
							return false;
						}
					}
					else {
						i+=1;
					}
					
				}
			}
			return false;
		}
		
		public boolean buscarCanton(String canton, int indice) {
			ArrayNode arregloCantones = (ArrayNode) nodoAlertas.get("cantones");
			String llave = String.valueOf(indice);
			if(arregloCantones != null) {
				int j = 0;
				while (j < arregloCantones.get(0).get(llave).size()) {
					JsonNode alertacant = arregloCantones.get(0).get(llave);
					String Canton = alertacant.get(j).asText();
					if (Canton.equals(canton)) {
							return true;
						}
					else {
						j+=1;
					}			
				}
					
			}	
			return false;
		}
		
		public boolean buscarDistrito(String distrito) {
			ArrayNode arregloDistritos = (ArrayNode) nodoAlertas.get("distritos");
			if(arregloDistritos != null) {
				int i = 0;
				while (i < arregloDistritos.get(0).size()) {
					String clave = String.valueOf(i+1);
					JsonNode alertadist = arregloDistritos.get(0);
					String Distrito = alertadist.get(clave).asText();
					if (Distrito.equals(distrito)) {
							return true;
						}
					else {
						i+=1;
					}
				}	
			}
			return false;
		}			
}
