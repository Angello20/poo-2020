// Angello Vividea García 
// POO GR 20
// 2019156710






import java.time.LocalDate;

public class Main {
	
	public static void main(String[] args) {
		
		//creamos la fecha de creación
		LocalDate fechaDeCreacion = LocalDate.parse("2020-05-29");
		
		//creamos una cuenta
		Cuenta miCuenta = new Cuenta(1122, 500.000, 0.045, fechaDeCreacion);
		
		//creamos una cuenta le depositamos y retiramos dinero y consultamos el balance y la fecha de creació n
		System.out.println(miCuenta.getBalance());
		System.out.println(miCuenta.depositarDinero(150));
		System.out.println(miCuenta.retirarDinero(200));
		System.out.println(miCuenta.getBalance());
		System.out.println(miCuenta.getfechaDeCreacion());
		
		//Creo una cuenta y consulto el balance y la fecha de creación
		Cuenta miCuenta2 = new Cuenta(1520, 10000.000,0.045,fechaDeCreacion);
		System.out.println(miCuenta2.getBalance());
		System.out.println(miCuenta2.getfechaDeCreacion());
	}
}
