
public class Main {

	public static void main(String[] args) {
		/*
		Cliente cliente = new Cliente("Ricardo");
		Cajera cajera = new Cajera("Saúl");
		long tiempoInicial = System.currentTimeMillis();
		Cajera.procesarCompra(cliente, tiempoInicial);
	*/
	Cliente cliente = new Cliente("Samanta");
	Cliente cliente2 = new Cliente("Pedro");
	Cliente cliente3 = new Cliente("Ana");
	Cliente cliente4 = new Cliente("Rodrigo");

	long tiempoInicial = System.currentTimeMillis();
	
	CajeraThread cajera1 = new CajeraThread("Marta", cliente, tiempoInicial);
	CajeraThread cajero1 = new CajeraThread("Daniel", cliente2, tiempoInicial);
	CajeraThread cajero2 = new CajeraThread("Leonardo", cliente3, tiempoInicial);
	CajeraThread cajera2 = new CajeraThread("Carolina", cliente3, tiempoInicial);
	
	cajera1.start();
	cajero1.start();
	cajero2.start();
	cajera2.start();
	
	}
}
