package View;

import javafx.fxml.FXML;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;
import Idea.Casos;
import Idea.JSONParser;
import Idea.ListaDeCasos;
import Idea.ListaUsuarios;


public class Controller {
	
	private int Sesion;
	
	@FXML
    private Label Nombre;
	
	@FXML
    private Label Lugar;
    
    @FXML
    private Label Placa;
    
    @FXML
    private Label Alerta;
 
    @FXML
    private LineChart<?,?> chart;
    
    @FXML
    private CategoryAxis x;
    
    @FXML
    private NumberAxis y;
    
    @FXML
    private Label Mortalidad;
    
    @FXML
    private Label Recuperacion;
    
    @FXML
    private void initialize() {}
    private void establecerInfo() {
    	JSONParser parser = new JSONParser();
    	ListaDeCasos casos = parser.cargarCasos();
    	XYChart.Series Acumulados = new XYChart.Series<>();
    	XYChart.Series Activos = new XYChart.Series<>();
    	XYChart.Series Recuperados = new XYChart.Series<>();
    	int contador = 50;
    	while (contador < 50) {
    		Activos.setName("Activos");
    		Recuperados.setName("Recuperados");
    		Acumulados.setName("Acumulados");
    		Acumulados.getData().add(new XYChart.Data<>(casos.get(contador).getFecha(),casos.get(contador).getAcumulados()));
    		Activos.getData().add(new XYChart.Data<>(casos.get(contador).getFecha(),casos.get(contador).getActivos()));
    		Recuperados.getData().add(new XYChart.Data<>(casos.get(contador).getFecha(),casos.get(contador).getRecuperados()));
    		contador +=1;
    	}
    	
    chart.getData().addAll(Acumulados,Activos,Recuperados);
    Mortalidad.setText("Tasa de mortalidad: "+casos.get(0).getTasaMortalidad()+"%");
	Recuperacion.setText("Tasa de recuperacion: "+casos.get(0).getTasaRecuperacion()+"%");
	
	ListaUsuarios usuario = parser.cargarUsuarios();
	Nombre.setText(usuario.get(Sesion).getNombre());
    Lugar.setText(usuario.get(Sesion).getLugar().toString() + " se encuentra en " + usuario.get(Sesion).getAlerta().getTipoAlerta()+".");
    int recorreEstablecimientos = 0;
    String alertatemp = "";
    
    while(recorreEstablecimientos < usuario.get(Sesion).getAlerta().getEstablecimientos().size()) { 	
    	alertatemp += ("�" + usuario.get(Sesion).getAlerta().getEstablecimientos().get(recorreEstablecimientos) +".\n");
    	recorreEstablecimientos += 1;
    }
    
    Alerta.setText(alertatemp);
    int placatemp = usuario.get(Sesion).getPlaca();
    String placa = String.valueOf(placatemp);
    int recorreRestricciones = 0;
    String restriccion = "";
    
    while(recorreRestricciones < usuario.get(Sesion).getAlerta().getRestriccionVehicular().size()) {
    	restriccion += ("�" + usuario.get(Sesion).getAlerta().getRestriccionVehicular().get(recorreRestricciones) +".\n");
    	recorreRestricciones += 1;
    }
    
    Placa.setText("El vehiculo con la placa " + placa + " puede circular: \n" + restriccion);
}
    
    public void determinarSesion(int Sesion) {
    	this.Sesion = Sesion;
    	establecerInfo();
    }

	}