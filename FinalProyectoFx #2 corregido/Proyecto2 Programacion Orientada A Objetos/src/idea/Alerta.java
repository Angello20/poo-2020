package idea;

import java.util.ArrayList;

public class Alerta {
	
	String tipoAlerta;
	
	ArrayList<String> establecimientos;
	
	ArrayList<String> restriccionVehicular;
	
	
	public Alerta(String tipoAlerta, ArrayList<String> establecimientos, ArrayList<String> restriccionVehicular) {
		this.tipoAlerta = tipoAlerta;
		this.establecimientos = establecimientos;
		this.restriccionVehicular = restriccionVehicular;
	}
	public Alerta() {
		
	}
	
	//getters y setters
	public String getTipoAlerta() {
		return tipoAlerta;
	}
	public void setTipoAlerta(String tipoAlerta) {
		this.tipoAlerta = tipoAlerta;
	}
	public ArrayList<String> getEstablecimientos() {
		return establecimientos;
	}
	public void setEstablecimientos(ArrayList<String> establecimientos) {
		this.establecimientos = establecimientos;
	}
	public ArrayList<String> getRestriccionVehicular() {
		return restriccionVehicular;
	}
	public void setRestriccionVehicular(ArrayList<String> restriccionVehicular) {
		this.restriccionVehicular = restriccionVehicular;
	}
}
