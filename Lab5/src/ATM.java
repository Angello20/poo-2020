import javax.swing.JOptionPane;
import java.time.LocalDate;
import java.util.Scanner;

public class ATM {
	
	public static void main(String[] args) {
		
		// Creamos la fecha de creaciòn de las cuentas
		LocalDate fechaDeCreacion = LocalDate.parse("2020-05-29");
		
		// Creamos las 10 cuentas
		Cuenta miCuenta = new Cuenta(0, 100.000, 0.045, fechaDeCreacion);
		Cuenta miCuenta2 = new Cuenta(1, 100.000, 0.045, fechaDeCreacion);
		Cuenta miCuenta3 = new Cuenta(2, 100.000, 0.045, fechaDeCreacion);
		Cuenta miCuenta4 = new Cuenta(3, 100.000, 0.045, fechaDeCreacion);
		Cuenta miCuenta5 = new Cuenta(4, 100.000, 0.045, fechaDeCreacion);
		Cuenta miCuenta6 = new Cuenta(5, 100.000, 0.045, fechaDeCreacion);
		Cuenta miCuenta7 = new Cuenta(6, 100.000, 0.045, fechaDeCreacion);
		Cuenta miCuenta8 = new Cuenta(7, 100.000, 0.045, fechaDeCreacion);
		Cuenta miCuenta9 = new Cuenta(8, 100.000, 0.045, fechaDeCreacion);
		Cuenta miCuenta10 = new Cuenta(9, 100.000, 0.045, fechaDeCreacion);
		
		//variables a utilizar
		Scanner sn = new Scanner(System.in);
		int opcion = 0;
		int opcion2 = 0;
		int Numcuenta = 0;
		int dineroRetirado = 0;
		int dineroDepositado = 0;
		boolean salir = false;
		boolean salir2 = false;
		
		//registra la id de la cuenta 
		while(!salir) {
		System.out.println("Ingrese su id: ");
		Numcuenta = sn.nextInt();
		switch(Numcuenta) {
		case 0:
			opcion = 0;
			salir = true;
			break;
		case 1:
			opcion = 1;
			salir = true;
			break;
		case 2:
			opcion = 2;
			salir = true;
			break;
		case 3:
			opcion = 3;
			salir = true;
			break;
		case 4:
			opcion = 4;
			salir = true;
			break;
		case 5:
			opcion = 5;
			salir = true;
			break;
		case 6:
			opcion = 6;
			salir = true;
			break;
		case 7:
			opcion = 7;
			salir = true;
			break;
		case 8:
			opcion = 8;
			salir = true;
			break;
		case 9:
			opcion = 9;
			salir = true;
			break;
		}
		}
	    salir = false;
	    
	    //Despliega el menu principal del cajero y sus opciones
	    while(!salir) {
	    	
	    	//despliega las opciones del menu
			System.out.println("1. Ver el balance actual");
			System.out.println("2. Retirar Dinero");
			System.out.println("3. Depositar Dinero");
			System.out.println("4. Salir");
			
			//guarda la opcion del usuario
			System.out.println("Ingrese su selección: ");
			opcion2 = sn.nextInt();
			
			switch(opcion2) {
			
			//Muestra el balance actual de las 9 cuentas creadas
			case 1:
				if (opcion == 0) {
					System.out.println(miCuenta.getBalance());
				}
				if (opcion == 1) {
					System.out.println(miCuenta2.getBalance());
				}
				if (opcion == 2) {
					System.out.println(miCuenta3.getBalance());
				}
				if (opcion == 3) {
					System.out.println(miCuenta4.getBalance());
				}
				if (opcion == 4) {
					System.out.println(miCuenta5.getBalance());
				}
				if (opcion == 5) {
					System.out.println(miCuenta6.getBalance());
				}
				if (opcion == 6) {
					System.out.println(miCuenta7.getBalance());
				}
				if (opcion == 7) {
					System.out.println(miCuenta8.getBalance());
				}
				if (opcion == 8) {
					System.out.println(miCuenta9.getBalance());
				}
				if (opcion == 9) {
					System.out.println(miCuenta10.getBalance());
				}
				break;
				
			//Opción para retirar dinero de cada una de las 9 cuentas
			case 2:
				if (opcion == 0) {
					
					//Guarda la cantidad de dinero a retirar
					System.out.println("Ingrese la cantidad a retirar;");
					dineroRetirado = sn.nextInt();
				}
				
				//Si el dinero retirado es menor al balance lo retira y muestra el balance actual 
				if (dineroRetirado<miCuenta.getBalance()) {
					System.out.println(miCuenta.retirarDinero(dineroRetirado));
					System.out.print("Balance actual:");
				}
				
				//Si el dinero retirado es mayor al balance, muestra un mensaje de error
				if (dineroRetirado>miCuenta.getBalance()) {
						System.out.println("No se puede retirar dicho monto");
					} 
				if (opcion == 1) {
					System.out.println("Ingrese la cantidad a retirar;");
					dineroRetirado = sn.nextInt();
				}
				if (dineroRetirado<miCuenta2.getBalance()) {
					System.out.println(miCuenta2.retirarDinero(dineroRetirado));
					System.out.print("Balance actual:");
				}
				if (dineroRetirado>miCuenta2.getBalance()) {
						System.out.println("No se puede retirar dicho monto");
					} 
				if (opcion == 2) {
					System.out.println("Ingrese la cantidad a retirar;");
					dineroRetirado = sn.nextInt();
				}
				if (dineroRetirado<miCuenta3.getBalance()) {
					System.out.println(miCuenta3.retirarDinero(dineroRetirado));
					System.out.print("Balance actual:");
				}
				if (dineroRetirado>miCuenta3.getBalance()) {
						System.out.println("No se puede retirar dicho monto");
					} 
				if (opcion == 3) {
					System.out.println("Ingrese la cantidad a retirar;");
					dineroRetirado = sn.nextInt();
				}
				if (dineroRetirado<miCuenta4.getBalance()) {
					System.out.println(miCuenta4.retirarDinero(dineroRetirado));
					System.out.print("Balance actual:");
				}
				if (dineroRetirado>miCuenta4.getBalance()) {
						System.out.println("No se puede retirar dicho monto");
					} 
				if (opcion == 4) {
					System.out.println("Ingrese la cantidad a retirar;");
					dineroRetirado = sn.nextInt();
				}
				if (dineroRetirado<miCuenta5.getBalance()) {
					System.out.println(miCuenta5.retirarDinero(dineroRetirado));
					System.out.print("Balance actual:");
				}
				if (dineroRetirado>miCuenta5.getBalance()) {
						System.out.println("No se puede retirar dicho monto");
					} 
				if (opcion == 5) {
					System.out.println("Ingrese la cantidad a retirar;");
					dineroRetirado = sn.nextInt();
				}
				if (dineroRetirado<miCuenta6.getBalance()) {
					System.out.println(miCuenta6.retirarDinero(dineroRetirado));
					System.out.print("Balance actual:");
				}
				if (dineroRetirado>miCuenta6.getBalance()) {
						System.out.println("No se puede retirar dicho monto");
					} 
				if (opcion == 6) {
					System.out.println("Ingrese la cantidad a retirar;");
					dineroRetirado = sn.nextInt();
				}
				if (dineroRetirado<miCuenta7.getBalance()) {
					System.out.println(miCuenta7.retirarDinero(dineroRetirado));
					System.out.print("Balance actual:");
				}
				if (dineroRetirado>miCuenta7.getBalance()) {
						System.out.println("No se puede retirar dicho monto");
					} 
				if (opcion == 7) {
					System.out.println("Ingrese la cantidad a retirar;");
					dineroRetirado = sn.nextInt();
				}
				if (dineroRetirado<miCuenta8.getBalance()) {
					System.out.println(miCuenta8.retirarDinero(dineroRetirado));
					System.out.print("Balance actual:");
				}
				if (dineroRetirado>miCuenta8.getBalance()) {
						System.out.println("No se puede retirar dicho monto");
					} 
				if (opcion == 8) {
					System.out.println("Ingrese la cantidad a retirar;");
					dineroRetirado = sn.nextInt();
				}
				if (dineroRetirado<miCuenta9.getBalance()) {
					System.out.println(miCuenta9.retirarDinero(dineroRetirado));
					System.out.print("Balance actual:");
				}
				if (dineroRetirado>miCuenta9.getBalance()) {
						System.out.println("No se puede retirar dicho monto");
					} 
				if (opcion == 9) {
					System.out.println("Ingrese la cantidad a retirar;");
					dineroRetirado = sn.nextInt();
				}
				if (dineroRetirado<miCuenta10.getBalance()) {
					System.out.println(miCuenta10.retirarDinero(dineroRetirado));
					System.out.print("Balance actual:");
				}
				if (dineroRetirado>miCuenta10.getBalance()) {
						System.out.println("No se puede retirar dicho monto");
					} 
				break;
				
			//Opción para depositar a cada una de las 9 cuentas
			case 3:
				if (opcion == 0) {
					
					//Guarda la cantidad a depositar
					System.out.println("Ingrese la cantidad a depositar;");
					dineroDepositado = sn.nextInt();
					
					//Deposita la cantidad y muestra el balance actual
					System.out.println(miCuenta.depositarDinero(dineroDepositado));
					System.out.print("Balance actual:");
				}
				if (opcion == 1) {
					System.out.println("Ingrese la cantidad a depositar;");
					dineroDepositado = sn.nextInt();
					System.out.println(miCuenta2.depositarDinero(dineroDepositado));
					System.out.print("Balance actual:");
				}
				if (opcion == 2) {
					System.out.println("Ingrese la cantidad a depositar;");
					dineroDepositado = sn.nextInt();
					System.out.println(miCuenta3.depositarDinero(dineroDepositado));
					System.out.print("Balance actual:");
				}
				if (opcion == 3) {
					System.out.println("Ingrese la cantidad a depositar;");
					dineroDepositado = sn.nextInt();
					System.out.println(miCuenta4.depositarDinero(dineroDepositado));
					System.out.print("Balance actual:");
				}
				if (opcion == 4) {
					System.out.println("Ingrese la cantidad a depositar;");
					dineroDepositado = sn.nextInt();
					System.out.println(miCuenta5.depositarDinero(dineroDepositado));
					System.out.print("Balance actual:");
					}
				if (opcion == 5) {
					System.out.println("Ingrese la cantidad a depositar;");
					dineroDepositado = sn.nextInt();
					System.out.println(miCuenta6.depositarDinero(dineroDepositado));
					System.out.print("Balance actual:");
				}
				if (opcion == 6) {
					System.out.println("Ingrese la cantidad a depositar;");
					dineroDepositado = sn.nextInt();
					System.out.println(miCuenta7.depositarDinero(dineroDepositado));
					System.out.print("Balance actual:");
				}
				if (opcion == 7) {
					System.out.println("Ingrese la cantidad a depositar;");
					dineroDepositado = sn.nextInt();
					System.out.println(miCuenta8.depositarDinero(dineroDepositado));
					System.out.print("Balance actual:");
				}
				if (opcion == 8) {
					System.out.println("Ingrese la cantidad a depositar;");
					dineroDepositado = sn.nextInt();
					System.out.println(miCuenta9.depositarDinero(dineroDepositado));
					System.out.print("Balance actual:");
				}
				if (opcion == 9) {
					System.out.println("Ingrese la cantidad a depositar;");
					dineroDepositado = sn.nextInt();
					System.out.println(miCuenta10.depositarDinero(dineroDepositado));
					System.out.print("Balance actual:");
				}
				break;
				
			//Sale del sistema
			case 4:
				salir = true;
				break;
			}
		}


	}
} 
