import java.util.Base64;

public class Usuario {
	
	private String username;
	private String contraseña;
	private int id;
	private static int contadorUsuarios = 0;
	
	public Usuario(String username, String contraseña) {
		super();
		this.username = username;
		this.contraseña = contraseña;
		this.id = ++contadorUsuarios;
	}
	public String toString() {
		String texto = id + " " + username + " " + contraseña;
		return texto;
	}
	public void setId (int id) {
		this.id = 0;
	}
	public int getId() {
		return id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getContraseña() {
		this.contraseña = Base64.getEncoder().encodeToString(contraseña.getBytes());
		return contraseña;
	}
	public void setContraseña(String contraseña) {
		this.contraseña = contraseña;
	}
	
}
